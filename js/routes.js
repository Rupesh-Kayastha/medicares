var checkout_address;
var checkout_paymetmethod;
var checkout_emimehod;
var montlySubscription; 
routes = [
 {
    path: '/profile/',
    componentUrl: './pages/profile.html',
	
  },
  {
    path: '/addAddress/',
    componentUrl: './pages/addAddress.html',
    async: getAllAddress,
   beforeEnter: function (routeTo, routeFrom, resolve, reject) {
   var router = this;
   var app = router.app; 
   app.tab.show("#view-home");
   app.panel.left.close();
   app.panel.right.close();
   if($(".back").trigger( "click" )){
				
					resolve();
			}
			else{
				reject();
			}
   }
  },
  {
    path: '/cart/',
	async: loadCartDetails,
	on:{
		pageAfterIn:function(event,page){
		 montlySubscription = app.toggle.get('#monthlysbuscribe');
		 if(montlySubscription){
			 montlySubscription.on("change",function(){
				  if (montlySubscription.checked) {
					  setMontlySubscription(1);
					}
					else{
						setMontlySubscription(0);
					}
			  });
		 }		  
		},
		pageInit: function (event, page) {
          
        },
        pageBeforeRemove: function (event, page) {
           montlySubscription=null;
        },
	},
	
	
    
	
  },  
  {
	name:'home',
    path: '/',
    componentUrl: './pages/home.html',
	
  },
  {
	path: '/checkout/',
	async: checkout, 
	on: {
        
        pageAfterIn: function (event, page) {
          app.toolbar.hide('.toolbar');
		  checkout_address=app.smartSelect.get('#select-address');
		  checkout_paymetmethod=app.smartSelect.get('#select-payment');
		  checkout_emimehod=app.smartSelect.get('#select-emi');
		  checkout_address.setValue(0);
		  checkout_paymetmethod.setValue(0);
		  checkout_emimehod.setValue(0);
		  checkout_address.on("close",function(){
			  setCheckoutAddress(checkout_address.getValue());
		  });
		  checkout_paymetmethod.on("close",function(){
			  setCheckoutPayment(checkout_paymetmethod.getValue());
		  });
		  checkout_emimehod.on("close",function(){
			  setCheckoutEmi(checkout_emimehod.getValue());
		  });
        },
        pageInit: function (event, page) {
          
        },
        pageBeforeRemove: function (event, page) {
          app.toolbar.show('.toolbar');
		  checkout_address=null;
		  checkout_paymetmethod=null;
		  checkout_emimehod=null;
        },
    },
    
	beforeEnter: function (routeTo, routeFrom, resolve, reject) {
    var router = this;
	var app = router.app; 
	app.tab.show("#view-cart");
	app.panel.left.close();
	app.panel.right.close();
		if($(".back").trigger( "click" )){
		    
				resolve();
		}
		else{
			reject();
		}
    }
  },
  {
    path: '/about/',
    url: './pages/about.html',
  },
  {
    path: '/category/:id/:CategoryName/:pageindex',
	async: loadCategoryDetails,
	beforeEnter: function (routeTo, routeFrom, resolve, reject) {
    var router = this;
	var app = router.app; 
	app.tab.show("#view-home");
	app.panel.left.close();
	app.panel.right.close();
		if($(".back").trigger( "click" )){
		    
				resolve();
		}
		else{
			reject();
		}
    }
	
	
  },
  {
    path: '/product/:id/',
	componentUrl: './pages/product.html',
  },
  {
	  path: '/allorder/',
	  async: getAllOrders,
	  beforeEnter: function (routeTo, routeFrom, resolve, reject) {
		var router = this;
		var app = router.app; 
		app.tab.show("#view-home");
		app.panel.left.close();
		app.panel.right.close();
			if($(".back").trigger( "click" )){
				
					resolve();
			}
			else{
				reject();
			}
		
		
    }
  },
  {
	  path: '/subscription/',
	  async: getAllSubscription,
	  beforeEnter: function (routeTo, routeFrom, resolve, reject) {
		var router = this;
		var app = router.app; 
		app.tab.show("#view-home");
		app.panel.left.close();
		app.panel.right.close();
			if($(".back").trigger( "click" )){
				
					resolve();
			}
			else{
				reject();
			}
		
		
    }
  },
  {
	  path: '/myemi/',
	  async: getMyEmi,
	  beforeEnter: function (routeTo, routeFrom, resolve, reject) {
		var router = this;
		var app = router.app; 
		app.tab.show("#view-home");
		app.panel.left.close();
		app.panel.right.close();
			if($(".back").trigger( "click" )){
				 
					resolve();
			}
			else{
				reject();
			}
		
		
    }
  },
  {
	  path: '/orderdetails/:id/:OrderIdentifier/',
	  async: orderdetails,
	  beforeEnter: function (routeTo, routeFrom, resolve, reject) {
		var router = this;
		var app = router.app; 
		app.tab.show("#view-home");
		app.panel.left.close();
		app.panel.right.close();
			if($(".back").trigger( "click" )){
				 
					resolve();
			}
			else{
				reject();
			}
		
		
    }
  },
  {
    path: '/settings/',
    url: './pages/settings.html',
  },
  // Page Loaders & Router
  {
    path: '/page-loader-template7/:user/:userId/:posts/:postId/',
    templateUrl: './pages/page-loader-template7.html',
  },
  {
    path: '/page-loader-component/:user/:userId/:posts/:postId/',
    componentUrl: './pages/page-loader-component.html',
  },
  {
    path: '/request-and-load/user/:userId/',
    async: function (routeTo, routeFrom, resolve, reject) {
      // Router instance
      var router = this;

      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = routeTo.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: 'Vladimir',
          lastName: 'Kharlampidi',
          about: 'Hello, i am creator of Framework7! Hope you like it!',
          links: [
            {
              title: 'Framework7 Website',
              url: 'http://framework7.io',
            },
            {
              title: 'Framework7 Forum',
              url: 'http://forum.framework7.io',
            },
          ]
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            componentUrl: './pages/request-and-load.html',
          },
          {
            context: {
              user: user,
            }
          }
        );
      }, 1000);
    },
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
