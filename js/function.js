var API_URL='http://onestoppharma.in/api/';
var currentUser=null;
var currentChatToken=null;

var lasttime='';
var chatrefresh;
var orderreview;
var refreshwalletbalance_interval;

function checkout(routeTo, routeFrom, resolve, reject) {
	
	var router = this;
	var app = router.app; 
	var checkouthelper={
		status:0,
		message:"Data Not Fetched",
		data:[]
	};
  
  
	currentUser=JSON.parse(localStorage.getItem('currentUser'));
 	app.preloader.show();
	app.request.post(
		API_URL+'cart/checkouthelper',
		{employeeid:currentUser.employeeid},
		function (data) {
			
			checkouthelper=data;
			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/checkout.html'

				},
				// Custom template context
				{
					context: {
						helper:checkouthelper
					},
				
				}
			);
		},
		function(){
			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/checkout.html'

				},
				// Custom template context
				{
					context: {
						helper:checkouthelper
					},
					
				}
			);
		},
		'json'
	);
	
	
}
function getMyEmi (routeTo, routeFrom, resolve, reject) {
	var router = this;
	currentUser=JSON.parse(localStorage.getItem('currentUser'));
	var app = router.app; 
	
	var emi={
		EMI:[],
		Orders:[]
	};
	app.preloader.show();
	
	app.request.post(
		API_URL+'employee/getemi',
		{employeeid:currentUser.employeeid},
		function (data) {
			
			emi={
					EMI:data.data.EMI,
					Orders:data.data.Orders
				};

			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/emidetail.html'

				},
				// Custom template context
				{
					context: {
						emidetail:emi
					},
				
				}
			);
		},
		function(){
			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/emidetail.html'

				},
				// Custom template context
				{
					context: {
						emidetail:emi
					},
					
				}
			);
		},
		'json'
	);
	
	
}
function loadCartDetails(routeTo, routeFrom, resolve, reject) {
	var router = this;
	
	var app = router.app; 
	
	var cart={
		status:0,
		message:"Cart is Empty",
		cartdetail:[]
	};
  
  
	currentUser=JSON.parse(localStorage.getItem('currentUser'));
	if(currentUser){
 	app.preloader.show();
	app.request.post(
		API_URL+'cart/cartdetail',
		{Employeeid : currentUser.employeeid },
		function (data) {
			cart=data;

			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/cart.html'

				},
				// Custom template context
				{
					context: {
						cart:cart
					},
				
				}
			);
		},
		function(){
			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/cart.html'

				},
				// Custom template context
				{
					context: {
						cart:cart
					},
					
				}
			);
		},
		'json'
	);
	}
	else{
		resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/cart.html'

				},
				// Custom template context
				{
					context: {
						cart:cart
					},
					
				}
			);
	}
}
function orderdetails(routeTo, routeFrom, resolve, reject) {
	var router = this;
	
	var app = router.app; 
	
	var order={
		id:routeTo.params.id,
		OrderIdentifier:routeTo.params.OrderIdentifier,
		data:[]
	};
  
   
 	app.preloader.show();
	app.request.post(
		API_URL+'order/orderdetail',
		{orderid : routeTo.params.id },
		function (data) {
			order={
					id:routeTo.params.id,
					OrderIdentifier:routeTo.params.OrderIdentifier,
					data:data.orderdetail
				};

			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/orderdetail.html'

				},
				// Custom template context
				{
					context: {
						orderdetail:order
					},
				
				}
			);
		},
		function(){
			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/orderdetail.html'

				},
				// Custom template context
				{
					context: {
						orderdetail:order
					},
					
				}
			);
		},
		'json'
	);
}
function getAllSubscription(routeTo, routeFrom, resolve, reject) {
	var router = this;
	
	var app = router.app; 
	
	var subscription={
		orders: []
	};
  
   
 	app.preloader.show();
	app.request.post(
		API_URL+'order/allsubscriptions',
		{Employeeid : currentUser.employeeid },
		function (data) {
			subscription={
				
				orders: data.orders
			};
			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/subscription.html'

				},
				// Custom template context
				{
					context: {
						subscription:subscription
					},
				
				}
			);
		},
		function(){
			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/allorder.html'

				},
				// Custom template context
				{
					context: {
						subscription:subscription
					},
					
				}
			);
		},
		'json'
	);
}
function getAllAddress(){
	var orders={
		allorder: []
	};
  
   
 	app.preloader.show();
	app.request.post(
		API_URL+'order/allorder',
		{Employeeid : currentUser.employeeid },
		function (data) {
			orders={
				
				allorder: data.orders
			};
			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/allorder.html'

				},
				// Custom template context
				{
					context: {
						allOrders:orders
					},
				
				}
			);
		});
}
function getAllOrders(routeTo, routeFrom, resolve, reject) {
	var router = this;
	
	var app = router.app; 
	
	var orders={
		allorder: []
	};
  
   
 	app.preloader.show();
	app.request.post(
		API_URL+'order/allorder',
		{Employeeid : currentUser.employeeid },
		function (data) {
			orders={
				
				allorder: data.orders
			};
			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/allorder.html'

				},
				// Custom template context
				{
					context: {
						allOrders:orders
					},
				
				}
			);
		},
		function(){
			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/allorder.html'

				},
				// Custom template context
				{
					context: {
						allOrders:orders
					},
					
				}
			);
		},
		'json'
	);
}

function loadCategoryDetails(routeTo, routeFrom, resolve, reject) {
	
	var router = this;
	
	var app = router.app; 
	
	
  
   
 	app.preloader.show();
	var currentCategory={
		CategoryId: routeTo.params.id,
		CategoryName: routeTo.params.CategoryName,
		CategoryProducts: []
	};
	
	app.request.post(
		API_URL+'category/categoryproducts',
		{categoryid : routeTo.params.id,currentpage:routeTo.params.pageindex },
		function (data) {
			currentCategory={
				CategoryId: (routeTo.params.id==1)?0:routeTo.params.id,
				CategoryName: routeTo.params.CategoryName,
				CategoryProducts: data.data.products,
				SubCategories: data.data.categories,
                TotalProducs:data.data.TotalProducts,
                TotalPage:data.data.TotalPage,
                CurrentPage:data.data.CurrentPage,
                NextPage:data.data.NextPage,
                PrevPage:data.data.PrevPage
			};
			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/category.html'

				},
				// Custom template context
				{
					context: {
						category:currentCategory
					},
				
				}
			);
		},
		function(){
			app.preloader.hide();
			resolve(
				// How and what to load: template
				{
						   componentUrl: './pages/category.html'

				},
				// Custom template context
				{
					context: {
						category:currentCategory
					},
					
				}
			);
		},
		'json'
	);
}

function fetchmessage(t=0,initial=false)
{
	
    $.ajax({
			  url:API_URL+"chat/fetchmessage",
			  type: 'POST',
			  data:{lasttime:lasttime,Tickettoken:currentChatToken,EmployeeId:currentUser.employeeid},
               success:function(result)
               {
				   app.preloader.hide();
                                
                if(result.status)
                {
                    var html='';
                    $.each(result.data.allmessage,function(i,it){
                        lasttime=it.FullTime;
                        var imgcl=(it.isme==0)?'-right':'';
						var userimg=(it.isme==0)?'user.png':'support.png';
                       	html+='<div class="card demo-facebook-card">'+
									'<div class="card-header">'+
										'<div class="demo-facebook-avatar'+imgcl+'">'+
											'<img src="images/'+userimg+'" width="34" height="34"/>'+
										'</div>'+
										'<div class="demo-facebook-name'+imgcl+'">'+it.username+'</div>'+
										'<div class="demo-facebook-date'+imgcl+'">'+it.Time+'</div>'+
									'</div>'+
									'<div class="card-content card-content-padding"><p>'+it.message+'</p></div>'+
								'</div>';
                    });
                    $("#chat_content").append(html);
                    
					var TotalScrollHeight=$("#chat_content").prop("scrollHeight");
					var CurentScroll=$("#chat_content").scrollTop();
					var ChatHeight=$("#chat_content").height();
					
					if(TotalScrollHeight-CurentScroll<ChatHeight+20)
					$("#chat_content").animate({ scrollTop: $("#chat_content").prop("scrollHeight")}, 1000);
					
					if(initial)
					{
						$("#chat_content").animate({ scrollTop: $("#chat_content").prop("scrollHeight")}, 1000);
					}
                }
                
				
				
                if( (t==0 && $("#chat_content").html()!='') || (t==0 && lasttime==''))
                {
				
					chatrefresh=setTimeout(function(){ fetchmessage(0,false); },1000);
                }
               }
    });
}

function sendMessage(imagestring=false,fileURI=""){
	var message=$("#chat_input").val();
	
	if(message.trim()!=''){
		
		$.ajax({
			url: API_URL+'chat/messagesend',
			type: 'POST',
			crossDomain : true,
			cache: false,
			data:{Tickettoken:currentChatToken,EmployeeId:currentUser.employeeid,message:message},	
               success:function(result)
               {
                $('#chat_input').val('');
				
					if(result.status)
					{
						$("#chat_input").val('');
						//fetchmessage(1,true);
						if(imagestring){
							
							uploadCameraFileDoc(fileURI,result.status);
						}
						
					}
			   }
				
            });
	}	
}

function onTabShow(tab) {
  
  var view=app.views.get(tab);
 if(view){
	app.view.current.router.refreshPage();
	$(view.selector+" .page-content").scrollTop(0);
 }

}

async function cartReview(CartIdentifire){
	
	app.preloader.show();
	
	
	
	var result = await $.ajax({
			url: API_URL+'cart/cartdetail',
			type: 'POST',
			crossDomain : true,
			cache: false,
			data:{CartIdentifire:CartIdentifire}	
              			
            });
	
	if(result.status){
	var items_string="";
	var items=result.cartdetail.item;
	var PaymentType=result.cartdetail.PaymentType;
	var TotalPrice=(PaymentType==4)?result.cartdetail.RegularTotalPrice:result.cartdetail.DiscountedTotalPrice;
	
		items.forEach(function(item) {
		 
			var up=(PaymentType==4)?item.CartItemRegularPrice:item.CartItemDiscountedPrice;
			var tp=(PaymentType==4)?item.CartItemRegularRowTotal:item.CartItemDiscountedRowTotal;
			items_string+=`<tr>
							<td class="label-cell">`+item.CartItemName+`</td>
							<td class="numeric-cell">`+item.CartItemQty+`</td>
							<td class="numeric-cell">`+up+`</td>
							<td class="numeric-cell">`+tp+`</td>
						  </tr>`;
			
		});
		items_string+=`<tr>
							<th class="numeric-cell" colspan="3">Grand Total</th>
							<th class="numeric-cell">`+TotalPrice+`</th>
					   </tr>`;
	
	var orderdetails=`<table>
						<thead>
							<tr>
								<th class="label-cell">Name</th>
								<th class="numeric-cell">Qty</th>
								<th class="numeric-cell">Unit Price</th>
								<th class="numeric-cell">Total</th>
							</tr>
						</thead>
						<tbody>`+items_string+							
						`</tbody>
				  </table>`;
	var oreviewdtable;
	  orderreview=app.sheet.create({
		backdrop: true,
		closeByBackdropClick:false,
		content:  `<div class="sheet-modal orderreview" id="orderreview">
					  <div class="view">
						<div class="page">
						 <div class="navbar">
							<div class="navbar-inner">
							 <div class="title">Order Review</div>
							  <div class="right">
								<a href="#" class="link sheet-close orderreview" id="orderreview-close"><i class="material-icons">close</i></a>
							  </div>
							</div>
						  </div>
						  <div class="page-content">
							<div class="block" id="orderreview_content">								
								<div class="card">
									<div class="card-header">Delivery Address</div>
									<div class="card-content card-content-padding">`+
									result.cartdetail.address.AddressLine1+`<br>`+
									result.cartdetail.address.AddressLine2+`<br>`+
									result.cartdetail.address.LandMark+`<br>`+
									result.cartdetail.address.City+`<br>`+
									result.cartdetail.address.State+`<br>`+
									result.cartdetail.address.State+`-`+result.cartdetail.address.Zipcode+`<br>`+
									result.cartdetail.address.ContactNo
									+`</div>				
								</div>						
								<div class="card data-table data-table-init oreview">
									<div class="card-header">
									Cart Items
									</div>
									<div class="card-content">`
									+ orderdetails +
									`</div>				
								</div>
								<div class="card">
									<div class="card-header">Payment Details</div>
									<div class="card-content card-content-padding">`+
									`<b>Payment Method:</b> `+result.cartdetail.PaymentMethod+`<br>`+
									`<b>Emi Plan Name:</b> `+result.cartdetail.EMI.EmiPlanName+`<br>`+
									`<b>Emi Plan Period:</b> `+result.cartdetail.EMI.EmiPlanPeriod+`<br>`+
									`<b>Emi Amount:</b> `+result.cartdetail.EMI.EmiAmount+`<br>`+
									`</div>				
								</div>
								<div class="card">
									<div class="card-content card-content-padding">
										<p class="row">
											<button class="col button button-raised button-fill" onclick="finalreview('`+result.cartdetail.CartIdentifire+`')">Confirm Order</button>
										</p>									
									</div>
								</div>
							</div>
						  </div>
						  
						</div>
					  </div>
					</div>`,
		opened: function (sheet) {
			
			app.preloader.hide();
		},
		
		close: function(sheet){
		
		orderreview.destroy();
		},
		
		
		
	});
	
	
	orderreview.open();
	
	}
	else{
			app.dialog.alert("Opps. Something Went Wrong.")
		
	}
	
}


async function finalreview(CartIdentifire){
	
	app.dialog.confirm('Are you sure about confirming the details?', 'Order Confirm',async function(){
	
		app.preloader.show();
		  var result= await  $.ajax({
				 url: API_URL+'cart/revieworderconfirm',
				 type: 'POST',
				 crossDomain : true,
				 cache: false,
				 data:{CartIdentifire:CartIdentifire}
				 
		});
	
	
		app.preloader.hide();
		app.dialog.alert(result.message);
		if(result.status){
			
			orderreview.close();
			chat.close();
			app.views.main.router.navigate("/orderdetails/"+ result.orderid+"/"+result.OrderIdentifier +"/");
		}
		
	
	});
	
	
}

async function refreshwalletbalance(){	
	 
		currentUser=JSON.parse(localStorage.getItem('currentUser'));
		
		var result= await  $.ajax({
				 url: API_URL+'employee/getemployeedetails',
				 type: 'POST',
				 crossDomain : true,
				 cache: false,
				 data:{employeeid:currentUser.employeeid}
				 
		});
		
		
			if(result.status){
				
				localStorage.setItem('currentUser',JSON.stringify(result.data));
				currentUser=result.data;
				$("#mycreditbalance").html("&#x20B9; "+currentUser.creditbalance);
				
			}
		
		refreshwalletbalance_interval=setTimeout(refreshwalletbalance, 5000);
}
async function addtocart(MedicineId,type=1){	
		app.preloader.show();
		currentUser=JSON.parse(localStorage.getItem('currentUser'));
		
		var result= await  $.ajax({
				 url: API_URL+'cart/additem',
				 type: 'POST',
				 crossDomain : true,
				 cache: false,
				 data:{employeeid:currentUser.employeeid,medicineid:MedicineId}
				 
		});
		app.preloader.hide();
		if(result.status){
			if(type==1)
			app.dialog.alert("Item Added");
			else{
			app.dialog.alert("Qty Updated");
			app.view.current.router.refreshPage();
			}
			
		}
}
var removeItemId=null;
async function updateItem(CartQty,UpdateQty,MedicineId){
	
	finalQty=CartQty+UpdateQty;
	if(finalQty==0)
	{
		
		removeItem(MedicineId);
		return;
	}
	if(UpdateQty==1)
	addtocart(MedicineId,2);
	else{
		app.preloader.show();
		currentUser =JSON.parse(localStorage.getItem('currentUser'));
		
		var result  = await  $.ajax({
				 url: API_URL+'cart/updatequantity',
				 type: 'POST',
				 crossDomain : true,
				 cache: false,
				 data:{employeeid:currentUser.employeeid,medicineid:MedicineId,qty:CartQty}
				 
		});
		app.preloader.hide();
		if(result.status){
			app.dialog.alert("Qty Updated");
			app.view.current.router.refreshPage();
		}
	}
	
}
async function removeItemFinal(){
	
		app.preloader.show();
		currentUser =JSON.parse(localStorage.getItem('currentUser'));
		
		var result  = await  $.ajax({
				 url: API_URL+'cart/removeitem',
				 type: 'POST',
				 crossDomain : true,
				 cache: false,
				 data:{employeeid:currentUser.employeeid,medicineid:removeItemId}
				 
		});
		app.preloader.hide();
		if(result.status){
			app.dialog.alert("Item Removed");
			app.view.current.router.refreshPage();
		}
	removeItemId=null;
}
function stayItem(){
	removeItemId=null;
}
function removeItem(MedicineId){
	removeItemId=MedicineId;
	app.dialog.confirm("Are You Sure!", "Remove Item", removeItemFinal,stayItem);
}
function HideCartPlaceOrder(){
	
	$$("#regular_placeorder").hide();
	$$("#discounted_placeorder").hide();
}
function ShowCartPlaceOrder(){
	var address	= checkout_address.getValue();
	var payment	= checkout_paymetmethod.getValue();
	var emi		= checkout_emimehod.getValue();

	if(parseInt(address) && parseInt(payment)){
		
		if(parseInt(payment)!=4){
			$$("#regular_placeorder").hide();
			$$("#discounted_placeorder").show();
		}
		else{
			if(parseInt(emi)){
			$$("#regular_placeorder").show();
			$$("#discounted_placeorder").hide();
			}
			else{
				HideCartPlaceOrder();
			}
		}
	}
	else{
		HideCartPlaceOrder();
	}
}
async function setMontlySubscription(MontlySubscriptionStatus){
	
		
		app.preloader.show();
		currentUser =JSON.parse(localStorage.getItem('currentUser'));
		
		var result  = await  $.ajax({
				 url: API_URL+'cart/setmontlysubscription',
				 type: 'POST',
				 crossDomain : true,
				 cache: false,
				 data:{employeeid:currentUser.employeeid,montlysubscription:parseInt(MontlySubscriptionStatus)}
				 
		});
		app.preloader.hide();
		app.dialog.alert(result.msg);	
}
async function setCheckoutAddress(addressid){
	HideCartPlaceOrder();
	if(parseInt(addressid)){	
		app.preloader.show();
		currentUser =JSON.parse(localStorage.getItem('currentUser'));
		
		var result  = await  $.ajax({
				 url: API_URL+'cart/setaddress',
				 type: 'POST',
				 crossDomain : true,
				 cache: false,
				 data:{employeeid:currentUser.employeeid,addressid:addressid}
				 
		});
		app.preloader.hide();
		app.dialog.alert(result.msg);
		if(!result.status){
			
			HideCartPlaceOrder();
		}
		else{
			ShowCartPlaceOrder();
		}
	}
	else{
		HideCartPlaceOrder();
	}
}
async function setCheckoutPayment(paymenttype){
	HideCartPlaceOrder();
	
	
	if(parseInt(paymenttype)){	
		app.preloader.show();
		
		if(parseInt(paymenttype)==4){
		checkout_emimehod.setValue(0);
		$$("#emi_block").show();
		}
		else{
			$$("#emi_block").hide();
			
		}
		currentUser =JSON.parse(localStorage.getItem('currentUser'));
		
		var result  = await  $.ajax({
				 url: API_URL+'cart/setpayment',
				 type: 'POST',
				 crossDomain : true,
				 cache: false,
				 data:{employeeid:currentUser.employeeid,PaymentType:paymenttype}
				 
		});
		app.preloader.hide();
		app.dialog.alert(result.msg);
		if(!result.status){
			
			HideCartPlaceOrder();
		}
		else{
			if(parseInt(paymenttype)!=4){
			ShowCartPlaceOrder();
			}
		}
		
		
	}
	else{
		$$("#emi_block").hide();
		HideCartPlaceOrder();
	}
	
}
async function setCheckoutEmi(emiid){
	HideCartPlaceOrder();
	if(parseInt(emiid)){	
		app.preloader.show();
		currentUser =JSON.parse(localStorage.getItem('currentUser'));
		
		var result  = await  $.ajax({
				 url: API_URL+'cart/setemi',
				 type: 'POST',
				 crossDomain : true,
				 cache: false,
				 data:{employeeid:currentUser.employeeid,emiplanid:emiid,}
				 
		});
		app.preloader.hide();
		app.dialog.alert(result.msg);
		if(!result.status){
			
			HideCartPlaceOrder();
		}
		else{
			ShowCartPlaceOrder();
		}
	}
	else{
		HideCartPlaceOrder();
	}
}
async function placeorder(CartIdentifier){
	
	app.preloader.show();
	
	
	var result  = await  $.ajax({
			 url: API_URL+'cart/placeorder',
			 type: 'POST',
			 crossDomain : true,
			 cache: false,
			 data:{employeeid:currentUser.employeeid,CartIdentifier:CartIdentifier}
			 
	});
	app.preloader.hide();
	app.dialog.alert(result.msg);
	console.log(result);
	if(result.status){
		
		catalogView.router.back('/cart/', {force: true, ignoreCache: true, reload: true});
		app.views.main.router.navigate("/orderdetails/"+ result.orderid+"/"+result.OrderIdentifier +"/");
	}
	
	
	
}
function allnumeric(inputtxt)
   {
	
      var numbers = /^[0-9]+$/;
      if(inputtxt.value.match(numbers))
      {
      //alert('Your Registration number has accepted....');
      //document.form1.text1.focus();
      return true;
      }
      else
      {
      alert('Please input numeric characters only');
      //document.form1.text1.focus();
	  inputtxt.value='';
	  inputtxt.focus();
      return false;
      }
   }
async function addAddress(){
	

	pdata={Employeeid:currentUser.employeeid,
		AddressLine1:$('#NewAddress').find('#address1').val(),
		AddressLine2:$('#NewAddress').find('#address2').val(),
		LandMark:$('#NewAddress').find('#LandMark').val(),
		State:$('#NewAddress').find('#State').val(),
		City:$('#NewAddress').find('#City').val(),
		Zipcode:$('#NewAddress').find('#Zip').val(),
		ContactNo:$('#NewAddress').find('#ContactNo').val(),
			}
		app.preloader.show();
		  var result= await  $.ajax({
				 url: API_URL+'employee/addressadd',
				 type: 'POST',
				 crossDomain : true,
				 cache: false,
				 data:pdata
				 
		});
	
	
		app.preloader.hide();
		app.dialog.alert(result.message);
		if(result.status){
			app.tab.show("#view-home");
		app.panel.left.close();
		app.panel.right.close();
			app.views.main.router.navigate("/");
		}
		
	
	
}