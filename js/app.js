// Dom7
var $$ = Dom7;

// Framework7 App main instance
var app  = new Framework7({
  root: '#app', // App root element
  id: 'com.kriti.medicare', // App bundle ID
  name: 'Medicare', // App name
  theme: 'auto', // Automatic theme detection,
  pushState: true,
  dynamicNavbar: true,
  // view:{
     // pushState:true,

    // },
 // App root data
  data: function () {
    return;
  },
  // App root methods
  methods: {
	logout:function(){
		
		localStorage.removeItem('currentUser');
		app.panel.left.close();
		app.panel.right.close();
		app.views.main.router.navigate("/");
		app.tab.show("#view-home");
		app.methods.showlogin();
		clearTimeout(refreshwalletbalance_interval);
	},
	showlogin:function(){
		app.preloader.show();
		$("#myaccount_name").html("My Account");
		app.methods.getCompanies();
		
		if(!$$('#my-login-screen .login-button').hasClass("")){
			$$('#my-login-screen .login-button').addClass("not-active");
		}
		$$('#my-login-screen .otp-button').html("Send OTP");
		$('#my-login-screen #login-otp-box').hide();
	    $$('#my-login-screen [name="companyid"]').val('');
        $$('#my-login-screen [name="employeeid"]').val('');
        $$('#my-login-screen [name="otpcode"]').val('');
		app.loginScreen.open('#my-login-screen');
		
	},
    getCategoryMenu: function () {
     $.ajax({
			 url: API_URL+'category/allcategory',
			 type: 'POST',
			 crossDomain : true,
			 cache: false,
			 success: function(data) {
				 if(data.status){
				 var html = app.methods.BuildMenuTree(data.data,false,true,0);
				 $("#menu").html(html);
					 $("span.nav-sublist").on('click', function(){
						$(this).parents("label").siblings("ul").slideToggle();
						$(this).toggleClass("menu-active");
					 });
				}
				
			 }
		});
    },
	BuildMenuTree:function(data,isSub,root,lvl){	
		
		if(data){
		//var html = (isSub)?'<div>':''; // Wrap with div if true
		var html='';
		if(root)
		html += '<ul class="nav__list">';
		else
		html += '<ul class="group-list-'+lvl+'">';
		for(item in data){
			html += '<li>';
			if(typeof(data[item].items) === 'object' && data[item].items.length ){ // An array will return 'object'
				if(isSub){
					html += '<label for="sub-group-'+data[item].category_id+'"><a class="parent-link" href="/category/' + data[item].category_id + '/'+data[item].label+'/1">' + data[item].label + '</a><span class="material-icons nav-sublist">keyboard_arrow_right</span></label>';
				} else {
					html += '<label for="sub-group-'+data[item].category_id+'"><a class="parent-link" href="/category/' + data[item].category_id + '/'+data[item].label+'/1">' + data[item].label + '</a><span class="material-icons nav-sublist">keyboard_arrow_right</span></label>'; // Submenu found, but top level list item.
				}
				html += app.methods.BuildMenuTree(data[item].items, true,false,data[item].lvl); // Submenu found. Calling recursively same method (and wrapping it in a div)
			} else {
				html += '<a class="nav-link" href="/category/' + data[item].category_id + '/'+data[item].label+'/1">' + data[item].label + '</a>';	 // No submenu
			}
			html += '</li>';
		}
		html += '</ul>';
		//html += (isSub)?'</div>':'';
		return html;
		}
		
	},
	getCompanies:function(){
		
		 $.ajax({
			 url: API_URL+'company/allcompany',
			 type: 'POST',
			 crossDomain : true,
			 cache: false,
			 success: function(data) {
				 if(data.status){
					 var html='<option></option>';
					 for(item in data.data){
						
						 html+="<option value='"+data.data[item].CompanyId+"'>"+data.data[item].CompanyName+"</option>";
					
					 }
					 $("#login-company-list").html(html);
					 app.preloader.hide();
				 }
				
			 }
		});
	}
	
  },
  // App routes
  routes: routes,
});






// Init/Create views
var settingsView = app.views.create('#view-profile', {
  url: '/profile/'
});
var catalogView = app.views.create('#view-cart', {
  url: '/cart/'
});
var homeView = app.views.create('#view-home', {
  url: '/'
});

catalogView.on('show', function(){
 console.log(catalogView);
})

var cameradialog;
var presscription; 

function viewPresscription(ele){
	
	presscription= app.photoBrowser.create({
		photos: [$(ele).data("src")],
		on: {
			close: function () {
			  presscription.destroy()
			}
		  }
	});
	presscription.open();
}

var chat = app.popup.create({
  closeByBackdropClick:false,
  content:  '<div class="popup my-chat" id="my-chat">'+
			  '<div class="view">'+
				'<div class="page">'+
				  '<div class="navbar">'+
					'<div class="navbar-inner">'+
					  '<div class="title">Order By Support</div>'+
					  '<div class="right">'+
						'<a href="#" class="link popup-close my-chat" id="my-chat-close"><i class="material-icons">close</i></a>'+
					  '</div>'+
					'</div>'+
				  '</div>'+
				  '<div class="toolbar toolbar-bottom chat_box">'+
					'<div class="toolbar-inner">'+
						
								'<a class="link"><img src="images/attach.png" class="send-button attachment-select"  /></a>'+
								'<textarea id="chat_input" ></textarea>'+
								'<a class="link"><img src="images/send.png" onclick="sendMessage()" class="send-button" /></a>'+
									  
					'</div>'+
				  '</div>'+
				  '<div class="page-content">'+
					'<div class="block">'+
						'<div id="chat_content">'+
							
						'</div>'+
					'</div>'+
				  '</div>'+
				'</div>'+
			  '</div>'+
			'</div>',
  // Events
  on: {
    open: function (popup) {
		lasttime='';
		cameradialog=app.dialog.create({
			title: 'Attach Prescription',
			text: '',
			backdrop:true,
			cssClass:'attachment-dailog',
			closeByBackdropClick:true,
			buttons: [
			  {
				text: '<img src="images/camera.png"  />',
			  },
			  {
				text: '<img src="images/gallery.png"  />',
			  },
			],
			verticalButtons: false,
			onClick: function(dialog, index){
				if(index==0)
					capturePhoto(0);
				else if(index==1)
					capturePhoto(1);
				else if(index==2)
					app.dialog.alert("Pdf Upload will go here");	
				dialog.close();
			}
		  });
		$$('.attachment-select').on('click', function () {
		 cameradialog.open();
		});
		
		app.preloader.show();
		$.ajax({
			 url: API_URL+'chat/orderbysupport',
			 type: 'POST',
			 crossDomain : true,
			 cache: false,
			 data:{EmployeeId:currentUser.employeeid},
			 success: function(data) {
				 if(data.status){
					localStorage.setItem('currentChatToken',data.data.Tickettoken);
					currentChatToken=data.data.Tickettoken;
					var d = new Date();
                    var time=d.getHours()+':'+d.getMinutes();
					
					$("#chat_content").html(
					'<div class="card demo-facebook-card">'+
						'<div class="card-header">'+
							'<div class="demo-facebook-avatar">'+
								'<img src="images/support.png" width="34" height="34"/>'+
							'</div>'+
							'<div class="demo-facebook-name">Operator</div>'+
							'<div class="demo-facebook-date">'+time+'</div>'+
						'</div>'+
						'<div class="card-content card-content-padding">'+
							'<p>We are live and ready to chat with you now. Say something to start a live chat.</p>'+
						'</div>'+
					'</div>');
					app.preloader.hide();
				 }
				
			 }
		});
      
    },
	close: function(popup){
		clearTimeout(chatrefresh);
		cameradialog.destroy();
	},
    opened: function (popup) {
		
	  if(currentChatToken){
		  app.preloader.show();
			fetchmessage(0,true);
		}
    },
  }
});


// Login Screen Demo
$$('#my-login-screen .login-button').on('click', function () {
  var companyid = $$('#my-login-screen [name="companyid"]').val();
  var employeeid = $$('#my-login-screen [name="employeeid"]').val();
  var otpcode = $$('#my-login-screen [name="otpcode"]').val();

   if(!otpcode){
	  app.dialog.alert('Please Enter OTP');
	  return;
  }
  
  app.preloader.show();
   $.ajax({
		 url: API_URL+'login/userlogin',
		 type: 'POST',
		 crossDomain : true,
		 cache: false,
		 data:{companyid:companyid,employeeid:employeeid,otpcode:otpcode},
		 success: function(data) {
			app.preloader.hide();
			app.dialog.alert(data.message);
			if(data.status){
				app.loginScreen.close('#my-login-screen');	
				localStorage.setItem('currentUser',JSON.stringify(data.data));
				currentUser=data.data;
				$("#myaccount_name").html(currentUser.employename);
				refreshwalletbalance();
			}
			else{
				
			}
		 }
	});
  
});

$$('#my-login-screen .otp-button').on('click', function () {
  var companyid = $$('#my-login-screen [name="companyid"]').val();
  var employeeid = $$('#my-login-screen [name="employeeid"]').val();
  $$('#my-login-screen [name="otpcode"]').val('');
  if(!companyid){
	  app.dialog.alert('Please Select Your Company');
	  return;
  }
  if(!employeeid){
	  app.dialog.alert('Enter Your EmployeeId');
	  return;
  }
  
  app.preloader.show();
   $.ajax({
		 url: API_URL+'login/sendotp',
		 type: 'POST',
		 crossDomain : true,
		 cache: false,
		 data:{companyid:companyid,employeeid:employeeid},
		 success: function(data) {
			app.preloader.hide();
			app.dialog.alert(data.message);
			if(data.status){
				$$('#my-login-screen #login-otp-box').show();
				$$('#my-login-screen .login-button').removeClass("not-active");
				$$('#my-login-screen .otp-button').html("Re-Send OTP");				
			}
			else{
				$$('#my-login-screen #login-otp-box').hide();
			}
		 }
	});
 
});



// add handler
app.on('tabShow', onTabShow);
app.methods.getCategoryMenu();

if(!localStorage.getItem('currentUser')){
app.methods.showlogin();
}
else{
	currentUser=JSON.parse(localStorage.getItem('currentUser'));
	$("#myaccount_name").html(currentUser.employename);
	refreshwalletbalance();
}




