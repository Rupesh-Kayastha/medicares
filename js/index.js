/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var pictureSource;   // picture source
var destinationType; // sets the format of returned value

var CordovaApp = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
		pictureSource = navigator.camera.PictureSourceType;
		destinationType = navigator.camera.DestinationType;
		console.log(pictureSource);
		console.log(destinationType);
		
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
       

        console.log('Received Event: ' + id);
    }
};

CordovaApp.initialize();

var retries = 0;
function clearCache() {
    //navigator.camera.cleanup();
}

function uploadCameraFileDoc(fileURI,TicketChatID){
	 var win = function (result) {
        clearCache();
        retries = 0;
		console.log(result);
        alert('Done!');
    }
 
    var fail = function (error) {
        if (retries == 0) {
            retries ++;
            setTimeout(function() {
                uploadCameraFileDoc(fileURI,TicketChatID)
            }, 1000)
        } else {
            retries = 0;
            clearCache();
           	app.dialog.alert('Opps. Something wrong happens!');
        }
    }
 
    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
    options.mimeType = "image/jpeg";
    options.params = {TicketChatID:TicketChatID}; // if we need to send parameters to the server request
    var ft = new FileTransfer();
    ft.upload(fileURI, encodeURI(API_URL+'chat/prescriptionupload'), win, fail, options);
}
function onCapturePhoto(fileURI) {
	$("#chat_input").val('<div class="img_c"><img class="lazy lazy-fade-in" src="'+fileURI+'" data-src="'+fileURI+'" onclick="viewPresscription(this)" /></div>');
	sendMessage(true,fileURI);
}
 
function capturePhoto(type) {
	if(type==0){
    navigator.camera.getPicture(onCapturePhoto, onFail, {
        quality: 20,
        destinationType: destinationType.FILE_URI,
		sourceType : pictureSource.CAMERA,
		allowEdit : false,
		encodingType: Camera.EncodingType.JPEG,
		saveToPhotoAlbum: false,
		correctOrientation:true,
    });
	}
	else if(type==1){
		
		navigator.camera.getPicture(onCapturePhoto, onFail, {
        quality: 20,
        destinationType: destinationType.FILE_URI,
		sourceType : pictureSource.PHOTOLIBRARY,
		allowEdit : false,
		encodingType: Camera.EncodingType.JPEG,
		saveToPhotoAlbum: false,
		correctOrientation:true,
		});
	}
}
 
function onFail(message) {
	app.dialog.alert(message);
   
}